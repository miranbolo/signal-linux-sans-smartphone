# Signal sous linux sans smartphone

Encore une autre tutoriel pour installer et utiliser Signal sur Linux sans posséder de smartphone (mais un bigo au moins !)

**PDF visualisable en ligne** (mais pas de copier/coller pour les commandes): https://0xacab.org/miranbolo/signal-linux-sans-smartphone/-/blob/no-masters/signal_linux_bigophone.pdf 

**ODT pour pouvoir faire le copier/coller** : https://0xacab.org/miranbolo/signal-linux-sans-smartphone/-/blob/no-masters/signal_linux_bigophone.odt 


